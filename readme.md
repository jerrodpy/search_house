
## Laravel search house

### The task

#### API 
Using the provided CSV [data](/property-data.csv), create an API route using Laravel that allows the data to be searched. 
The data should be converted to a database table. Provide Laravel migrations and seeders within the project. 
The API should search on: 
Name: Should also match on partial names
Bedrooms: Exact match
Bathrooms: Exact match 
Storeys: Exact match 
Garages: Exact match 
Price: Range (between $X and $Y) 
 
All search parameters should be optional, we should be able to search for 2 bedroom houses, or 4 bedroom and 2 bathroom houses, etc. 
 
The API should return JSON, with pure numeric data (not HTML content). 
 
#### Frontend (Search Form) 
 
Create a basic search form that will query the API using AJAX and display the results it receives from the backend. The searching result should be rendering to HTML table dynamically on the frontend, using reactive Vue.js/Javascript

There should be some sort of searching indicator, a spinning icon or similar. 
 
 

## Installation

To install the project must be installed: composer, npm, yarn

 - composer install
 - npm install
 - npm run development
 - php artisan serve
 

After these steps, on the page http://127.0.0.1:8000 we see the search form, selecting the necessary criteria and clicking the search button below we get the result.
If you want to exclude from the filtering criteria, select the value "-", or 0 for the price.
By choosing the value 0 for parameters other than the price, we are looking for houses in which these parameters are missing (for example, if garages = 0, then houses are selected without a garage)
