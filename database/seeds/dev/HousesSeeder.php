<?php

use Illuminate\Database\Seeder;

/**
 * Class HousesSeeder
 */
class HousesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Houses::class, 150)->create();
    }
}
