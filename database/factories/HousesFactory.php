<?php
declare( strict_types = 1 );

use App\Models\Supplier;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Houses::class, function (Faker $faker) {
    return [
        'name'     => $faker->name,
        'price'    => $faker->numberBetween(100, 5000),
        'bedrooms' => $faker->numberBetween(0, 7),
        'bathrooms'=> $faker->numberBetween(0, 7),
        'storeys'  => $faker->numberBetween(0, 5),
        'garages'  => $faker->numberBetween(0, 5),
    ];
});