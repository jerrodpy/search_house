<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SearchHouseController@index')->name('login_post');
Route::get('/get-list-bedrooms', 'SearchHouseController@getAllOptions')->name('all.options');
Route::post('/ajax-search-house', 'SearchHouseController@ajaxSearchHouse')->name('ajax.search.house');