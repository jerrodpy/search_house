<?php
declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\AbstractRepoInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class AbstractRepo
 * @package App\Repositories\Eloquent
 */
class AbstractRepo implements AbstractRepoInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * AbstractRepo constructor.
     *
     * @param string $model
     */
    public function __construct(string $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->model::find($id);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function findOrFail($id)
    {
        return $this->model::findOrfail($id);
    }


    /**
     * @return mixed
     */
    public function paginate()
    {
        return $this->model::paginate();
    }

    /**
     * @param null $data
     *
     * @return mixed
     */
    public function get($data = null)
    {
        return $data !== null
            ? $this->model::where($data)
                ->get()
            : $this->model::get();
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model::all();
    }


    /**
     * @param string $column
     * @param array  $values
     *
     * @return mixed
     */
    public function getByColumn(string $column, array $values)
    {
        return $this->model::whereIn($column, $values)
            ->get();
    }

    /**
     * @param int $timeStamp
     *
     * @return string
     */
    public function timestampToCarbon($timeStamp): string
    {
        return Carbon::createFromTimestamp((int)$timeStamp)
            ->toDateTimeString();
    }
}
