<?php
declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Models\Houses;
use App\Repositories\Interfaces\HousesRepositoryInterface;
use Illuminate\Support\Collection;


/**
 * Class ProductRepository
 * @package App\Repositories\Eloquent
 */
class HousesRepository extends AbstractRepo implements HousesRepositoryInterface
{
    /**
     * ThemeRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(Houses::class);
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model::create($data);
    }

    /**
     * @param array  $data
     * @param Houses $house
     *
     * @return Houses
     */
    public function update(array $data, Houses $house): Houses
    {
        $house->update($data);

        return $house;
    }

    /**
     * @param Houses $house
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(Houses $house): bool
    {
        return $house->delete();
    }


    /**
     * @return array
     */
    public function getAllOptions() : array
    {
        $fieldName = [
            'bedrooms',
            'bathrooms',
            'storeys',
            'garages',
        ];

        $result = [];

        foreach ($fieldName as $item)
        {
            $result[$item] = $this->model::select($item)
                ->groupBy($item)
                ->pluck($item)
                ->all();
        }

        //TODO add data caching

        return $result;
    }

    /**
     * @param array $options
     *
     * @return Collection
     */
    public function searchHouse(array $options) :Collection
    {
        $query =  $this->model::query();

        if (isset($options['bedrooms'])) {
            $query->where('bedrooms', $options['bedrooms']);
        }

        if (isset($options['bathrooms'])) {
            $query->where('bathrooms', $options['bathrooms']);
        }

        if (isset($options['bedrooms'])) {
            $query->where('bedrooms', $options['bedrooms']);
        }

        if (isset($options['storeys'])) {
            $query->where('storeys', $options['storeys']);
        }

        if (isset($options['garages'])) {
            $query->where('garages', $options['garages']);
        }

        if (isset($options['priceFrom']) && $options['priceFrom'] ) {
            $query->where('price','>', $options['priceFrom'] );
        }

        if (isset($options['priceTo']) && $options['priceTo'] ) {
            $query->where('price', '<', $options['priceTo']);
        }

//        if (isset($options['take'])) {
//            $query->take($options['take']);
//        }
//
//        if (isset($options['skip'])) {
//            $query->skip($options['skip']);
//        }

        return $query->get();
    }

}
