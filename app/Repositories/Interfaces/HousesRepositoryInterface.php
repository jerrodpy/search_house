<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

use App\Models\Houses;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

/**
 * Interface GroupRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface HousesRepositoryInterface extends AbstractRepoInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array  $data
     * @param Houses $house
     *
     * @return Houses
     */
    public function update(array $data, Houses $house): Houses;

    /**
     * @param Houses $house
     *
     * @return bool
     */
    public function delete(Houses $house): bool;


    /**
     * @return array
     */
    public function getAllOptions(): array;

    /**
     * @param array $options
     *
     * @return Collection
     */
    public function searchHouse(array $options): Collection;

}
