<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

/**
 * Interface InsertOrUpdateInterface
 * @package App\Repositories\Interfaces
 */
interface InsertOrUpdateInterface
{
    /**
     * @param array $rows
     *
     * @return mixed
     */
    public function insertOrUpdateMany(array $rows);
}
