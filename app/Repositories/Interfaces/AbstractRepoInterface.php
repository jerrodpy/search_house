<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;

/**
 * Interface AbstractRepoInterface
 * @package App\Repositories\Interfaces
 */
interface AbstractRepoInterface
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function findOrFail($id);

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function find(int $id);


    /**
     * @return mixed
     */
    public function paginate();

    /**
     * @param null $data
     *
     * @return mixed
     */
    public function get($data = null);

    /**
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * @param string $column
     * @param array  $values
     *
     * @return mixed
     */
    public function getByColumn(string $column, array $values);

    /**
     * @param int $timeStamp
     *
     * @return string
     */
    public function timestampToCarbon($timeStamp): string;
}
