<?php
declare(strict_types=1);

namespace App\Providers;

use App\Repositories\Eloquent\HousesRepository;
use App\Repositories\Interfaces\HousesRepositoryInterface;
use Illuminate\Support\ServiceProvider;

/**
 * Class RepoServiceProvider
 * @package App\Providers
 */
class RepoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     */
    public function boot()
    {
    }

    /**
     * Register services.
     */
    public function register()
    {
        $this->app->bind(
            HousesRepositoryInterface::class,
            HousesRepository::class
        );


    }
}
