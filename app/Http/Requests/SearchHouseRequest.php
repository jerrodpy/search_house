<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SearchHouseRequest
 * @package App\Http\Requests\Quote
 */
class SearchHouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bathrooms'     => 'sometimes|integer|min:0',
            'storeys'       => 'sometimes|integer|min:0',
            'garages'       => 'sometimes|integer|min:0',
            'bedrooms'      => 'sometimes|integer|min:0',
            'priceFrom'     => 'sometimes|integer|min:0',
            'priceTo'       => 'sometimes|integer|min:0',
        ];
    }
}