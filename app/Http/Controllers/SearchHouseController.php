<?php
declare(strict_types=1);


namespace App\Http\Controllers;

use App\Http\Requests\SearchHouseRequest;
use App\Repositories\Interfaces\HousesRepositoryInterface;
use Illuminate\Http\JsonResponse;

/**
 * Class SearchHouseController
 * @package App\Http\Controllers
 */
class SearchHouseController extends Controller
{

    /**
     * @var HousesRepositoryInterface
     */
    private $repository;

    /**
     * SearchHouseController constructor.
     *
     * @param HousesRepositoryInterface $repository
     */
    public function __construct(HousesRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $options = $this->repository->getAllOptions();

        return view('welcome', compact('options'));
    }

    /**
     * @return JsonResponse
     */
    public function getAllOptions(): JsonResponse
    {
        $data = $this->repository->getAllOptions();

        return $this->successResponse($data);
    }


    /**
     * @param SearchHouseRequest $request
     *
     * @return \Illuminate\Support\Collection
     */
    public function ajaxSearchHouse(SearchHouseRequest $request)
    {
        $data = $request->validated();

        return $this->repository->searchHouse($data);
    }
}