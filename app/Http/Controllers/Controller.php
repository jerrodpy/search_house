<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param mixed       $data
     * @param string|null $message
     * @return JsonResponse
     */
    protected function successResponse($data = null, string $message = null) : JsonResponse
    {
        return JsonResponse::create([
            'message' => $message ?? 'Success',
            'data'    => $data,
        ], 200);
    }
}
